# Crossposter

This is a Python script that allows you to post a message to both Twitter and Mastodon at the same time.

## Requirements

To use this script, you will need the following:

- A Twitter and Mastodon developer account. You can get your developer credentials at https://developer.twitter.com/ and https://docs.joinmastodon.org/api/getting-started/

- The required libraries are listed in the "requirements.txt" file. You can install them with `pip install -r requirements.txt`

## Usage

To use this script, follow these steps:

1. Rename the "config.ini.example" file to "config.ini" and edit it with your Twitter and Mastodon credentials.

2. Run the script with `python crossposter.py -h` and follow the on-screen instructions to post your message. The `-h` flag will display the script's usage information.

3. Note that the maximum length of the text you can post to Twitter or Mastodon is currently 280 characters, due to Twitter's character limit. This limitation is temporary and will be lifted in a future update.

## License

This script is available under the [MIT](LICENSE) license.

#!/usr/bin/env python
import sys
import configparser
import os
import argparse
import tweepy
from mastodon import Mastodon

config = configparser.ConfigParser()
path = f"{os.path.dirname(__file__)}/config.ini"

def is_postable_in_Twitter(post):
    if len(post) > int(config['Limits']['Tweet_max_length']):
        print("Sorry, but your message is too long to be posted as a tweet. Please make sure your message is 280 characters or less.")
        return False
    return True

def is_postable_in_Mastodon(post):
    return True

def post_in_Twitter(post, image):
    try:
        if is_postable_in_Twitter(post):
            twitter_auth_keys = {
                "consumer_key"        : config['Config']['twitter_consumer_key'],
                "consumer_secret"     : config['Config']['twitter_consumer_secret'],
                "access_token"        : config['Config']['twitter_access_token'],
                "access_token_secret" : config['Config']['twitter_access_token_secret']
            }
        
            auth = tweepy.OAuthHandler(
                    twitter_auth_keys['consumer_key'],
                    twitter_auth_keys['consumer_secret']
                    )
            auth.set_access_token(
                    twitter_auth_keys['access_token'],
                    twitter_auth_keys['access_token_secret']
                    )
            api = tweepy.API(auth)
            
            if image == False:
                status = api.update_status(status=post)
            else:
                status = api.update_status_with_media(status=post, filename=image)
                
    except Exception as ex:
        print (f"Unexpected error:{ex}")
        
def post_in_Mastodon(post, image):
    try:
        if is_postable_in_Mastodon(post):
            m = Mastodon(access_token=config['Config']['mastodon_access_token'], api_base_url=config['Config']['mastodon_api_base_url'])
            if image == False:
                m.toot(post)
            else:
                media = m.media_post(image)
                m.status_post(status=post, media_ids=media)
                
    except Exception as ex:
        print(f"Unexpected error:{ex}")  
        
def main(post, skip_Twitter, skip_Mastodon, image):
        
        if image != False:
            if check_fileExist(args.image) == False:
                print("Sorry, the file could not be found.")
                exit()
                
        if skip_Twitter == False:
            post_in_Twitter(post,image)
            
        if skip_Mastodon == False:
            post_in_Mastodon(post,image)        
        
        print(f"Done!")
  
def check_fileExist(file):
    return os.path.isfile(file)

if __name__ == "__main__":
    config.read(path)
    
    parser = argparse.ArgumentParser(description='Crossposter, easily share your messages on both Twitter and Mastodon')
    parser.add_argument("-p","--post", help="Text to send, example: 'hello world' ", action="store", nargs=1)
    parser.add_argument("-st","--skipTwitter", help="Skip post on Twitter", action="store_true")
    parser.add_argument("-sm","--skipMastodon", help="Skip post on Mastodon", action="store_true")
    parser.add_argument("-i", "--image", help="Add image to status")
    
    args = parser.parse_args()
    
    if args.post:
        skip_Twitter = args.skipTwitter
        skip_Mastodon = args.skipMastodon
        image = args.image if args.image != None else False
        
        main(args.post[0], skip_Twitter, skip_Mastodon, image)